import config from './rollup.config.umd.js';
import {PATH_DIST} from './config.js';

config.output.format = 'es';
config.output.file   = PATH_DIST + 'index.esm.js';
export default config;