import { ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

export abstract class BootstrapDirective implements OnInit {

  private _display: string | null = null;

  public constructor(
    private renderer: Renderer2,
    protected readonly elementRef: ElementRef,
    private initClasses: string[],
    private allowedTags: string[] = ['DIV'],
  ) {
  }

  @Input('if')
  public set ifValue(value: boolean) {
    if (value) {
      let display: string = 'block';
      if (this._display) {
        display = this._display;
      }
      this.setStyle('display', display);
    } else if (value === false) {
      this._display = this.getStyle('display');
      this.setStyle('display', 'none');
    }
  }

  public ngOnInit(): void {
    for (const cssClass of this.initClasses) {
      this.addClass(cssClass);
    }
    if (this.allowedTags.indexOf(this.elementRef.nativeElement.tagName) === -1) {
      throw new Error(`this directive can not used on '${this.elementRef.nativeElement.tagName}' elements`);
    }
  }

  public addClass(cssClass: string): void {
    this.renderer.addClass(this.elementRef.nativeElement, cssClass);
  }

  public removeClass(cssClass: string): void {
    this.renderer.removeClass(this.elementRef.nativeElement, cssClass);
  }

  public setAttribute(attribute: string, value: string): void {
    this.renderer.setAttribute(this.elementRef.nativeElement, attribute, value);
  }

  public removeAttribute(attribute: string): void {
    this.renderer.removeAttribute(this.elementRef.nativeElement, attribute);
  }

  public setStyle(style: string, value: string): void {
    this.renderer.setStyle(this.elementRef.nativeElement, style, value);
  }

  public getStyle(style: string): string {
    return this.elementRef.nativeElement.style[style] || '';
  }

  public setProperty(name: string, value: string): void {
    this.renderer.setProperty(this.elementRef.nativeElement, name, value);
  }

  public setValue(value: string): void {
    this.renderer.setValue(this.elementRef.nativeElement, value);
  }

}
