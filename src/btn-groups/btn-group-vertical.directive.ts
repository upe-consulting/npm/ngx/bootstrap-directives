import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { BootstrapDirective } from '../bootstrap/bootstrap.directive';

@Directive({selector: '[upeBtnGroupVertical]'})
export class BtnGroupVerticalDirective extends BootstrapDirective implements OnInit {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['btn-group-vertical']);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.setAttribute('role', 'group');
  }

}
