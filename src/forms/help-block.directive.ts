import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import { BootstrapDirective, BootstrapVisibleDirective, IBootstrapVisibleDirective } from '../bootstrap';

@Directive({selector: '[upeHelpBlock]'})
export class HelpBlockDirective extends BootstrapDirective implements IBootstrapVisibleDirective {

  @Input('upeHelpBlock') public visible: boolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['help-block'], ['SPAN']);
  }

}

ApplyMixins(HelpBlockDirective, [BootstrapVisibleDirective]);
