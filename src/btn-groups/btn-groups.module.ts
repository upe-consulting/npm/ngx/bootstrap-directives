import { NgModule } from '@angular/core';
import { BtnGroupJustifiedDirective } from './btn-group-justified.directive';
import { BtnGroupVerticalDirective } from './btn-group-vertical.directive';
import { BtnGroupDirective } from './btn-group.directive';
import { BtnToolbarDirective } from './btn-toolbar.directive';

@NgModule({
  imports: [],
  exports: [
    BtnGroupDirective,
    BtnGroupJustifiedDirective,
    BtnGroupVerticalDirective,
    BtnToolbarDirective,
  ],
  declarations: [
    BtnGroupDirective,
    BtnGroupJustifiedDirective,
    BtnGroupVerticalDirective,
    BtnToolbarDirective,
  ],
  providers: [],
})
export class BtnGroupDirectivesModule {
}
