import { ElementRef, Input, Renderer2 } from '@angular/core';
import { BootstrapDirective } from './bootstrap.directive';
import { AttrBoolean, Size } from './types';

export abstract class BootstrapSizeDirective extends BootstrapDirective {

  private _baseName: string;

  public constructor(r: Renderer2, er: ElementRef, ic: string[], at?: string[], defaultSize?: Size) {
    if (defaultSize) {
      ic.push(`${ic[0]}-${defaultSize}`);
    }
    super(r, er, ic, at);
    this._baseName = ic[0];
    this._size = defaultSize ? defaultSize : null;
  }

  @Input()
  public set sm(value: AttrBoolean) {
    value || value === '' ? this.size = 'sm' : this.size = null;
  }

  @Input()
  public set lg(value: AttrBoolean) {
    value || value === '' ? this.size = 'lg' : this.size = null;
  }

  private _size: Size | null;

  @Input()
  public set size(value: Size | null) {
    if (this._size !== null) {
      this.removeClass(`${this._baseName}-${this._size}`);
    }
    this._size = value;
    if (value !== null) {
      this.addClass(`${this._baseName}-${this._size}`);
    }
  }

}
