import { Component, ViewChild } from '@angular/core';
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import {
  CheckboxDirective,
  CheckboxInlineDirective,
  ControlLabelDirective,
  FormControlDirective,
  FormDirectivesModule,
  FormGroupDirective,
  HelpBlockDirective,
  RadioDirective,
  RadioInlineDirective,
} from '../src/forms';
import { DirectivesSpec } from './directives';

@Component({
  template: `
              <form upeFormInline></form>
              <form upeFormHorizontal></form>
              <div upeFormGroup>
                <p upeFormControlStatic></p>
                <label upeControlLabel></label>
                <input type="password" upeFormControl>
                <label upeCheckboxInline></label>
                <label upeRadioInline></label>
                <div upeCheckbox></div>
                <div upeRadio></div>
                <span upeHelpBlock></span>
              </div>
            `,
})
class FormsTestComponent {
  @ViewChild(CheckboxDirective) public cbd: CheckboxDirective;
  @ViewChild(CheckboxInlineDirective) public cbid: CheckboxInlineDirective;
  @ViewChild(FormControlDirective) public fcd: FormControlDirective;
  @ViewChild(FormGroupDirective) public fgd: FormGroupDirective;
  @ViewChild(ControlLabelDirective) public cld: ControlLabelDirective;
  @ViewChild(RadioDirective) public rd: RadioDirective;
  @ViewChild(HelpBlockDirective) public hbd: HelpBlockDirective;
  @ViewChild(RadioInlineDirective) public rid: RadioInlineDirective;
}

@suite
class FormsSpec extends DirectivesSpec<FormsTestComponent> {

  public constructor() {
    super([FormDirectivesModule], FormsTestComponent);
  }

  @test
  public 'checkbox'(): void {
    this.minTest('checkbox');
    this.disabledTest('checkbox', this.i.cbd);
  }

  @test
  public 'upeCheckboxInline'(): void {
    this.minTest('CheckboxInline');
    this.disabledTest('CheckboxInline', this.i.cbid);
  }

  @test
  public 'upeControlLabel'(): void {
    this.minTest('ControlLabel');
    this.visibleTest('ControlLabel', this.i.cld);
  }

  @test
  public 'upeFormControl'(): void {
    const debugElement = this.minTest('FormControl');
    this.disabledTest('FormControl', this.i.fcd);

    expect(debugElement.attributes.readonly).undefined;
    this.i.fcd.readonly = true;
    expect(debugElement.attributes.readonly).not.undefined;
    this.i.fcd.readonly = false;
    expect(debugElement.attributes.readonly).null;

    expect(debugElement.classes[`input-lg`]).undefined;
    expect(debugElement.classes[`input-sm`]).undefined;

    this.i.fcd.size = 'lg';
    expect(debugElement.classes[`input-lg`]).true;
    expect(debugElement.classes[`input-sm`]).undefined;

    this.i.fcd.size = 'sm';
    expect(debugElement.classes[`input-sm`]).true;
    expect(debugElement.classes[`input-lg`]).false;

    this.i.fcd.size = null;
    expect(debugElement.classes[`input-sm`]).false;
    expect(debugElement.classes[`input-lg`]).false;

    this.i.fcd.sm = '';
    expect(debugElement.classes[`input-sm`]).true;
    expect(debugElement.classes[`input-lg`]).false;

    this.i.fcd.lg = '';
    expect(debugElement.classes[`input-sm`]).false;
    expect(debugElement.classes[`input-lg`]).true;

    this.i.fcd.lg = false;
    expect(debugElement.classes[`input-sm`]).false;
    expect(debugElement.classes[`input-lg`]).false;

    this.i.fcd.sm = true;
    expect(debugElement.classes[`input-sm`]).true;
    expect(debugElement.classes[`input-lg`]).false;

    this.i.fcd.lg = true;
    expect(debugElement.classes[`input-sm`]).false;
    expect(debugElement.classes[`input-lg`]).true;

    this.i.fcd.sm = false;
    expect(debugElement.classes[`input-sm`]).false;
    expect(debugElement.classes[`input-lg`]).false;
  }

  @test
  public 'upeFormGroup'(): void {
    const debugElement = this.minTest('FormGroup');
    this.sizeTest('FormGroup', this.i.fgd);

    expect(debugElement.classes[`has-error`]).undefined;
    expect(debugElement.classes[`has-warning`]).undefined;
    expect(debugElement.classes[`has-success`]).undefined;

    this.i.fgd.validationState = 'error';
    expect(debugElement.classes[`has-error`]).true;
    expect(debugElement.classes[`has-warning`]).undefined;
    expect(debugElement.classes[`has-success`]).undefined;

    this.i.fgd.validationState = 'warning';
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).true;
    expect(debugElement.classes[`has-success`]).undefined;

    this.i.fgd.validationState = 'success';
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).true;

    this.i.fgd.validationState = null;
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).false;

    expect(debugElement.classes['has-feedback']).undefined;
    this.i.fgd.hasFeedback = true;
    expect(debugElement.classes['has-feedback']).true;
    this.i.fgd.hasFeedback = false;
    expect(debugElement.classes['has-feedback']).false;
    this.i.fgd.hasFeedback = '';
    expect(debugElement.classes['has-feedback']).true;

    this.i.fgd.hasError = '';
    expect(debugElement.classes[`has-error`]).true;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).false;

    this.i.fgd.hasWarning = '';
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).true;
    expect(debugElement.classes[`has-success`]).false;

    this.i.fgd.hasSuccess = '';
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).true;

    this.i.fgd.hasWarning = true;
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).true;
    expect(debugElement.classes[`has-success`]).false;

    this.i.fgd.hasError = false;
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).false;

    this.i.fgd.hasSuccess = true;
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).true;

    this.i.fgd.hasWarning = false;
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).false;

    this.i.fgd.hasSuccess = false;
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).false;

    this.i.fgd.hasError = false;
    expect(debugElement.classes[`has-error`]).false;
    expect(debugElement.classes[`has-warning`]).false;
    expect(debugElement.classes[`has-success`]).false;

  }

  @test
  public 'upeFormHorizontal'(): void {
    this.minTest('FormHorizontal');
  }

  @test
  public 'upeFormInline'(): void {
    this.minTest('FormInline');
  }

  @test
  public 'upeHelpBlock'(): void {
    this.minTest('HelpBlock');
    this.visibleTest('HelpBlock', this.i.hbd);
  }

  @test
  public 'radio'(): void {
    this.minTest('radio');
    this.disabledTest('radio', this.i.rd);
  }

  @test
  public 'upeRadioInline'(): void {
    this.minTest('RadioInline');
    this.disabledTest('RadioInline', this.i.rid);
  }

}
