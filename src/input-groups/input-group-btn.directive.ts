import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import { BootstrapDirective, BootstrapVisibleDirective, IBootstrapVisibleDirective } from '../bootstrap';

@Directive({selector: '[upeInputGroupBtn]'})
export class InputGroupBtnDirective extends BootstrapDirective implements IBootstrapVisibleDirective {

  @Input('upeInputGroupBtn') public visible: boolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['input-group-btn'], ['SPAN']);
  }

}

ApplyMixins(InputGroupBtnDirective, [BootstrapVisibleDirective]);
