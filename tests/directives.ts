import { DebugElement } from '@angular/core';
import { ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { expect } from 'chai';
import { IBootstrapActiveDirective } from '../src/bootstrap/bootstrap-active.directive';
import { IBootstrapDisabledDirective } from '../src/bootstrap/bootstrap-disabled.directive';
import { BootstrapSizeDirective } from '../src/bootstrap/bootstrap-size.directive';
import { IBootstrapTextCenterDirective } from '../src/bootstrap/bootstrap-text-center.directive';
import { BootstrapTypeDirective } from '../src/bootstrap/bootstrap-type.directive';
import { IBootstrapVisibleDirective } from '../src/bootstrap/bootstrap-visible.directive';
import { BootstrapDirective } from '../src/bootstrap/bootstrap.directive';
import { ColorType } from '../src/bootstrap/types';

export abstract class DirectivesSpec<T> {

  public f: ComponentFixture<T>;
  public i: T;

  public constructor(private imports: any[], private testComponent: new () => T) {

  }

  public before(): void {

    TestBed.configureTestingModule({
      imports:      this.imports,
      declarations: [this.testComponent],
    });

    this.f = TestBed.createComponent(this.testComponent);
    this.f.detectChanges();
    this.i = this.f.componentInstance;
  }

  public getDirective(name: string): DebugElement {
    return this.f.debugElement.query(By.css(`[upe${name}]`));
  }

  public after(): void {
    getTestBed().resetTestingModule();
  }

  public minTest(name: string): DebugElement {
    const debugElement = this.getDirective(name);

    const cssName = this.toCss(name);

    expect(debugElement.classes[cssName]).true;

    return debugElement;
  }

  public typeTest(name: string, it: BootstrapTypeDirective, hasDefault: boolean = true): void {
    const debugElement       = this.getDirective(name);
    const types: ColorType[] = ['primary', 'success'];

    const cssName = this.toCss(name);

    if (hasDefault) {
      expect(debugElement.classes[`${cssName}-default`]).true;
    }

    for (const type of types) {
      it.styleType = type;
      expect(debugElement.classes[`${cssName}-${type}`]).true;
    }
  }

  public disabledTest(name: string, it: IBootstrapDisabledDirective): void {
    const debugElement = this.getDirective(name);

    it.disabled = true;
    expect(debugElement.classes['disabled']).true;
    expect(debugElement.attributes['disabled']).not.undefined;

    it.disabled = false;
    expect(debugElement.classes['disabled']).false;
    expect(debugElement.attributes['disabled']).null;
  }

  public activeTest(name: string, it: IBootstrapActiveDirective): void {
    const debugElement = this.getDirective(name);

    it.active = true;
    expect(debugElement.classes['active']).true;

    it.active = false;
    expect(debugElement.classes['active']).false;
  }

  public roleTest(name: string, role: string): void {
    const debugElement = this.getDirective(name);
    expect(debugElement.attributes.role).eq(role);
  }

  public textCenterTest(name: string, it: IBootstrapTextCenterDirective): void {
    const debugElement = this.getDirective(name);

    expect(debugElement.classes['text-center']).undefined;

    it.center = true;
    expect(debugElement.classes['text-center']).true;

    it.center = false;
    expect(debugElement.classes['text-center']).false;
  }

  public visibleTest(name: string, it: IBootstrapVisibleDirective & BootstrapDirective): void {
    const debugElement = this.getDirective(name);
    expect(debugElement.styles['display']).undefined;
    it.visible = false;
    expect(debugElement.styles['display']).eq('none');
    it.visible = true;
    expect(debugElement.styles['display']).eq('block');
    it.setStyle('display', 'inline');
    expect(debugElement.styles['display']).eq('inline');
    it.visible = false;
    expect(debugElement.styles['display']).eq('none');
    it.visible = true;
    expect(debugElement.styles['display']).eq('inline');
  }

  public ifValueTest(name: string, it: BootstrapDirective): void {
    const debugElement = this.getDirective(name);
    expect(debugElement.styles['display']).undefined;
    it.ifValue = '' as any;
    expect(debugElement.styles['display']).undefined;
    it.ifValue = false;
    expect(debugElement.styles['display']).eq('none');
    it.ifValue = true;
    expect(debugElement.styles['display']).eq('block');
    it.setStyle('display', 'inline');
    expect(debugElement.styles['display']).eq('inline');
    it.ifValue = false;
    expect(debugElement.styles['display']).eq('none');
    it.ifValue = true;
    expect(debugElement.styles['display']).eq('inline');
  }

  public sizeTest(name: string, it: BootstrapSizeDirective): void {
    const debugElement = this.getDirective(name);

    const cssName = this.toCss(name);

    expect(debugElement.classes[`${cssName}-lg`]).undefined;
    expect(debugElement.classes[`${cssName}-sm`]).undefined;

    it.size = 'lg';
    expect(debugElement.classes[`${cssName}-lg`]).true;
    expect(debugElement.classes[`${cssName}-sm`]).undefined;

    it.size = 'sm';
    expect(debugElement.classes[`${cssName}-sm`]).true;
    expect(debugElement.classes[`${cssName}-lg`]).false;

    it.size = null;
    expect(debugElement.classes[`${cssName}-sm`]).false;
    expect(debugElement.classes[`${cssName}-lg`]).false;

    it.sm = '';
    expect(debugElement.classes[`${cssName}-sm`]).true;
    expect(debugElement.classes[`${cssName}-lg`]).false;

    it.lg = '';
    expect(debugElement.classes[`${cssName}-sm`]).false;
    expect(debugElement.classes[`${cssName}-lg`]).true;

    it.lg = false;
    expect(debugElement.classes[`${cssName}-sm`]).false;
    expect(debugElement.classes[`${cssName}-lg`]).false;

    it.sm = true;
    expect(debugElement.classes[`${cssName}-sm`]).true;
    expect(debugElement.classes[`${cssName}-lg`]).false;

    it.lg = true;
    expect(debugElement.classes[`${cssName}-sm`]).false;
    expect(debugElement.classes[`${cssName}-lg`]).true;

    it.sm = false;
    expect(debugElement.classes[`${cssName}-sm`]).false;
    expect(debugElement.classes[`${cssName}-lg`]).false;
  }

  private toCss(name: string): string {
    return name.replace(/([A-Z])/g, (m: string, p1: string, offset: number) =>
      (offset !== 0 ? '-' : '') + p1.toLowerCase());
  }

}
