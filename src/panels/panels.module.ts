import { NgModule } from '@angular/core';
import { PanelBodyDirective } from './panel-body.directive';
import { PanelFooterDirective } from './panel-footer.directive';
import { PanelHeadingDirective } from './panel-heading.directive';
import { PanelTitleDirective } from './panel-title.directive';
import { PanelDirective } from './panel.directive';

@NgModule({
  imports: [],
  exports: [
    PanelDirective,
    PanelHeadingDirective,
    PanelTitleDirective,
    PanelBodyDirective,
    PanelFooterDirective,
  ],
  declarations: [
    PanelDirective,
    PanelHeadingDirective,
    PanelTitleDirective,
    PanelBodyDirective,
    PanelFooterDirective,
  ],
  providers: [],
})
export class PanelDirectivesModule {
}
