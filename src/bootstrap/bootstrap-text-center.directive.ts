import { BootstrapDirective } from './bootstrap.directive';
import { AttrBoolean } from './types';

export interface IBootstrapTextCenterDirective {
  center: AttrBoolean;
}

export abstract class BootstrapTextCenterDirective extends BootstrapDirective implements IBootstrapTextCenterDirective {
  public set center(value: AttrBoolean) {
    value || value === '' ? this.addClass('text-center') : this.removeClass('text-center');
  }
}
