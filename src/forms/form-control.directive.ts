import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import {
  AttrBoolean,
  BootstrapDirective,
  BootstrapDisabledDirective,
  IBootstrapDisabledDirective,
  Size
} from '../bootstrap';

@Directive({selector: '[upeFormControl]'})
export class FormControlDirective extends BootstrapDirective implements OnInit, IBootstrapDisabledDirective {

  public static SUPPORTED_TYPES: string[] = [
    'text',
    'password',
    'datetime',
    'datetime-local',
    'date',
    'month',
    'time',
    'week',
    'number',
    'email',
    'url',
    'search',
    'tel',
    'color',
  ];

  @Input() public disabled: AttrBoolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['form-control'], ['INPUT', 'SELECT', 'TEXTAREA']);
  }

  @Input()
  public set readonly(value: AttrBoolean) {
    value || value === '' ? this.setAttribute('readonly', '') : this.removeAttribute('readonly');
  }

  @Input()
  public set sm(value: AttrBoolean) {
    value || value === '' ? this.size = 'sm' : this.size = null;
  }

  @Input()
  public set lg(value: AttrBoolean) {
    value || value === '' ? this.size = 'lg' : this.size = null;
  }

  private _size: Size | null = null;

  @Input()
  public set size(value: Size | null) {
    if (this._size !== null) {
      this.removeClass(`input-${this._size}`);
    }
    this._size = value;
    if (this._size !== null) {
      this.addClass(`input-${this._size}`);
    }
  }

  public ngOnInit(): void {
    super.ngOnInit();
    if (this.elementRef.nativeElement.tagName === 'INPUT') {
      const type = this.elementRef.nativeElement.getAttribute('type');
      if (FormControlDirective.SUPPORTED_TYPES.indexOf(type) === -1) {
        throw new Error(`the input type '${type}' is not supported`);
      }
    }
  }

}

ApplyMixins(FormControlDirective, [BootstrapDisabledDirective]);
