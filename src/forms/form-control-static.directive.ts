import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { BootstrapDirective } from '../bootstrap';

@Directive({selector: '[upeFormControlStatic]'})
export class FormControlStaticDirective extends BootstrapDirective {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['form-control-static'], ['P']);
  }

}
