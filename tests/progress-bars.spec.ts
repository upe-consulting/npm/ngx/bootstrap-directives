import { Component, DebugElement, ViewChild } from '@angular/core';
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { ProgressBarDirective, ProgressBarDirectivesModule } from '../src/progress-bars';
import { DirectivesSpec } from './directives';

@Component({
  template: `
              <div upeProgress>
                <div upeProgressBar></div>
              </div>`,
})
class ProgressBarsTestComponent {
  @ViewChild(ProgressBarDirective) public pbd: ProgressBarDirective;
}

@suite
class ProgressBarsSpec extends DirectivesSpec<ProgressBarsTestComponent> {

  public pb: DebugElement;

  public constructor() {
    super([ProgressBarDirectivesModule], ProgressBarsTestComponent);
  }

  public before(): void {
    super.before();
    this.pb = this.getDirective('ProgressBar');
  }

  @test
  public 'progress'(): void {
    this.minTest('progress');
  }

  @test
  public 'upeProgressBar'(): void {
    expect(this.pb.classes['progress-bar']).true;
    expect(this.pb.attributes['aria-valuemax']).eq('100.00');
    expect(this.pb.attributes['aria-valuemin']).eq('0.00');
    expect(this.pb.attributes['aria-valuenow']).eq('0.00');
    expect(this.pb.attributes['role']).eq('progressbar');
    expect(this.pb.styles['width']).eq('0.00%');
    expect(this.pb.styles['minWidth']).undefined;
    expect(this.pb.classes['active']).undefined;
    expect(this.pb.classes['progress-bar-striped']).undefined;
  }

  @test
  public 'progress-bar set progress'(): void {
    const progress = 10;

    this.i.pbd.progress = progress;
    expect(this.pb.attributes['aria-valuenow']).eq(this.toFixed(progress));
    expect(this.pb.styles['width']).eq(`${this.toFixed(progress)}%`);

    const min      = 5;
    this.i.pbd.min = min;
    expect(() => this.i.pbd.progress = min - 1).throw;

    const max      = 60;
    this.i.pbd.max = max;
    expect(() => this.i.pbd.progress = max + 1).throw;

  }

  @test
  public 'progress-bar set min'(): void {
    const min = 5;

    this.i.pbd.min = min;
    expect(this.pb.attributes['aria-valuemin']).eq(this.toFixed(min));

    expect(this.pb.attributes['aria-valuenow']).eq(this.toFixed(min));
  }

  @test
  public 'progress-bar set max'(): void {
    const max = 50;

    this.i.pbd.progress = max + 1;
    this.i.pbd.max      = max;
    expect(this.pb.attributes['aria-valuemax']).eq(this.toFixed(max));

    expect(this.pb.attributes['aria-valuenow']).eq(this.toFixed(max));
  }

  @test
  public 'progress-bar set min width'(): void {
    const minWidth       = 5;
    const minWidthString = `${minWidth}%`;

    this.i.pbd.minWidth = minWidth;
    expect(this.pb.styles['min-width']).eq(`${minWidth}px`);

    this.i.pbd.minWidth = minWidthString;
    expect(this.pb.styles['min-width']).eq(minWidthString);
  }

  @test
  public 'striped'(): void {
    this.i.pbd.striped = true;
    expect(this.pb.classes['progress-bar-striped']).true;
    this.i.pbd.striped = false;
    expect(this.pb.classes['progress-bar-striped']).false;
  }

  @test
  public 'animated'(): void {
    this.i.pbd.animated = true;
    expect(this.pb.classes['active']).true;
    expect(this.pb.classes['progress-bar-striped']).true;

    this.i.pbd.animated = false;
    expect(this.pb.classes['active']).false;
    expect(this.pb.classes['progress-bar-striped']).false;

    this.i.pbd.striped  = true;
    this.i.pbd.animated = true;
    expect(this.pb.classes['active']).true;
    expect(this.pb.classes['progress-bar-striped']).true;

    this.i.pbd.animated = false;
    expect(this.pb.classes['active']).false;
    expect(this.pb.classes['progress-bar-striped']).true;
  }

  private toFixed(num: number): string {
    return num.toFixed(this.i.pbd.fraction);
  }

}
