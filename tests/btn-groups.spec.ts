import { Component, ViewChild } from '@angular/core';
import { suite, test } from 'mocha-typescript';
import { BtnGroupDirective, BtnGroupDirectivesModule } from '../src/btn-groups';
import { DirectivesSpec } from './directives';

@Component({
  template: `
              <div upeBtnGroup></div>
              <div upeBtnToolbar></div>
              <div upeBtnGroupVertical></div>
              <div upeBtnGroupJustified></div>`,
})
class BtnGroupsTestComponent {
  @ViewChild(BtnGroupDirective) public bgd: BtnGroupDirective;
}

@suite
class BtnGroupsSpec extends DirectivesSpec<BtnGroupsTestComponent> {

  public constructor() {
    super([BtnGroupDirectivesModule], BtnGroupsTestComponent);
  }

  @test
  public 'upeBtnGroup'(): void {
    this.minTest('BtnGroup');
    this.sizeTest('BtnGroup', this.i.bgd);
    this.roleTest('BtnGroup', 'group');
  }

  @test
  public 'upeBtnToolbar'(): void {
    this.minTest('BtnToolbar');
    this.roleTest('BtnToolbar', 'toolbar');
  }

  @test
  public 'upeBtnGroupVertical'(): void {
    this.minTest('BtnGroupVertical');
    this.roleTest('BtnGroupVertical', 'group');
  }

  @test
  public 'upeBtnGroupJustified'(): void {
    this.minTest('BtnGroupJustified');
    this.roleTest('BtnGroupJustified', 'group');
  }

}
