import { BootstrapDirective } from './bootstrap.directive';
import { AttrBoolean } from './types';

export interface IBootstrapActiveDirective {
  active: AttrBoolean;
}

export abstract class BootstrapActiveDirective extends BootstrapDirective implements IBootstrapActiveDirective {
  public set active(value: AttrBoolean) {
    value || value === '' ? this.addClass('active') : this.removeClass('active');
  }
}
