import { ElementRef, Input, Renderer2 } from '@angular/core';
import { BootstrapDirective } from './bootstrap.directive';
import { AttrBoolean, ColorType } from './types';

export abstract class BootstrapTypeDirective extends BootstrapDirective {

  private _baseName: string;

  public constructor(r: Renderer2, er: ElementRef, ic: string[], at?: string[], defaultType?: ColorType) {
    if (defaultType) {
      ic.push(`${ic[0]}-${defaultType}`);
    }
    super(r, er, ic, at);
    this._baseName = ic[0];
    this._styleType = defaultType ? defaultType : null;
  }

  @Input()
  public set primary(value: AttrBoolean) {
    value || value === '' ? this.styleType = 'primary' : this.styleType = null;
  }

  @Input()
  public set success(value: AttrBoolean) {
    value || value === '' ? this.styleType = 'success' : this.styleType = null;
  }

  @Input()
  public set danger(value: AttrBoolean) {
    value || value === '' ? this.styleType = 'danger' : this.styleType = null;
  }

  private _styleType: ColorType | null;

  @Input()
  public set styleType(value: ColorType | null) {
    if (this._styleType !== null) {
      this.removeClass(`${this._baseName}-${this._styleType}`);
    }
    this._styleType = value;
    if (this._styleType !== null) {
      this.addClass(`${this._baseName}-${this._styleType}`);
    }
  }

}
