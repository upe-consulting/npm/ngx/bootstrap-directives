import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import { BootstrapDirective, BootstrapVisibleDirective, IBootstrapVisibleDirective } from '../bootstrap';

@Directive({selector: '[upeInputGroupAddon]'})
export class InputGroupAddonDirective extends BootstrapDirective implements IBootstrapVisibleDirective {

  @Input('upeInputGroupAddon') public visible: boolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['input-group-addon'], ['SPAN']);
  }

}

ApplyMixins(InputGroupAddonDirective, [BootstrapVisibleDirective]);
