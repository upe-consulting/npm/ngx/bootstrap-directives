import { NgModule } from '@angular/core';
import { InputGroupAddonDirective } from './input-group-addon.directive';
import { InputGroupBtnDirective } from './input-group-btn.directive';
import { InputGroupDirective } from './input-group.directive';

@NgModule({
  imports: [],
  exports: [
    InputGroupDirective,
    InputGroupAddonDirective,
    InputGroupBtnDirective,
  ],
  declarations: [
    InputGroupDirective,
    InputGroupAddonDirective,
    InputGroupBtnDirective,
  ],
  providers: [],
})
export class InputGroupDirectivesModule {
}
