import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import { BootstrapDirective, BootstrapVisibleDirective, IBootstrapVisibleDirective } from '../bootstrap';

@Directive({selector: '[upeControlLabel]'})
export class ControlLabelDirective extends BootstrapDirective implements IBootstrapVisibleDirective {

  @Input('upeControlLabel') public visible: boolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['control-label'], ['LABEL']);
  }

}

ApplyMixins(ControlLabelDirective, [BootstrapVisibleDirective]);
