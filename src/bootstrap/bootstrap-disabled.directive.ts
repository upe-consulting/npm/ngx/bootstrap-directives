import { BootstrapDirective } from './bootstrap.directive';
import { AttrBoolean } from './types';

export interface IBootstrapDisabledDirective {
  disabled: AttrBoolean;
}

export abstract class BootstrapDisabledDirective extends BootstrapDirective implements IBootstrapDisabledDirective {
  public set disabled(value: AttrBoolean) {
    value || value === '' ? this.addClass('disabled') : this.removeClass('disabled');
    value || value === '' ? this.setAttribute('disabled', '') : this.removeAttribute('disabled');
  }
}
