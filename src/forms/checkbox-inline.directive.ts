import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import { AttrBoolean, BootstrapDirective, BootstrapDisabledDirective, IBootstrapDisabledDirective } from '../bootstrap';

@Directive({selector: '[upeCheckboxInline]'})
export class CheckboxInlineDirective extends BootstrapDirective implements IBootstrapDisabledDirective {

  @Input() public disabled: AttrBoolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['checkbox-inline'], ['LABEL']);
  }

}

ApplyMixins(CheckboxInlineDirective, [BootstrapDisabledDirective]);
