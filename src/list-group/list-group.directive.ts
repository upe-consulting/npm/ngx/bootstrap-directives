import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { BootstrapDirective } from '../bootstrap';

@Directive({selector: '[upeListGroup]'})
export class ListGroupDirective extends BootstrapDirective {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['list-group'], ['UL']);
  }

}
