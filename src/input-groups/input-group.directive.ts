import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { BootstrapSizeDirective } from '../bootstrap';

@Directive({selector: '[upeInputGroup]'})
export class InputGroupDirective extends BootstrapSizeDirective {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['input-group']);
  }

}
