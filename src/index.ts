import { NgModule } from '@angular/core';
import { BtnGroupDirectivesModule } from './btn-groups';
import { ButtonDirectivesModule } from './buttons';
import { FormDirectivesModule } from './forms';
import { InputGroupDirectivesModule } from './input-groups';
import { ListGroupDirectivesModule } from './list-group';
import { PanelDirectivesModule } from './panels';
import { ProgressBarDirectivesModule } from './progress-bars';

export * from './btn-groups';
export * from './buttons';
export * from './forms';
export * from './input-groups';
export * from './list-group';
export * from './panels';
export * from './progress-bars';
export * from './bootstrap';

@NgModule({
  declarations: [],
  imports: [],
  providers: [],
  exports: [
    BtnGroupDirectivesModule,
    ButtonDirectivesModule,
    InputGroupDirectivesModule,
    ListGroupDirectivesModule,
    PanelDirectivesModule,
    ProgressBarDirectivesModule,
    FormDirectivesModule,
  ],
})
export class NgxBootstrapDirectivesModule {
}
