import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import {
  AttrBoolean,
  BootstrapDisabledDirective,
  BootstrapTypeDirective,
  IBootstrapDisabledDirective
} from '../bootstrap';

@Directive({selector: '[upeListGroupItem]'})
export class ListGroupItemDirective extends BootstrapTypeDirective implements IBootstrapDisabledDirective {

  @Input() public disabled: AttrBoolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['list-group-item'], ['LI']);
  }

}

ApplyMixins(ListGroupItemDirective, [BootstrapDisabledDirective]);
