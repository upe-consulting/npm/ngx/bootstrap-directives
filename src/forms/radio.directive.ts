import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import { AttrBoolean, BootstrapDirective, BootstrapDisabledDirective, IBootstrapDisabledDirective } from '../bootstrap';

@Directive({selector: '[upeRadio]'})
export class RadioDirective extends BootstrapDirective implements IBootstrapDisabledDirective {

  @Input() public disabled: AttrBoolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['radio']);
  }

}

ApplyMixins(RadioDirective, [BootstrapDisabledDirective]);
