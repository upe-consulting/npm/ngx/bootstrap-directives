import { Component, ViewChild } from '@angular/core';
import { suite, test } from 'mocha-typescript';
import { PanelDirective, PanelDirectivesModule, PanelTitleDirective } from '../src/panels';
import { DirectivesSpec } from './directives';

@Component({
  template: `
              <div upePanel>
                <div upePanelHeading>
                  <h3 upePanelTitle></h3>
                </div>
                <div upePanelBody></div>
                <div upePanelFooter></div>
              </div>`,
})
class PanelsTestComponent {
  @ViewChild(PanelDirective) public pd: PanelDirective;
  @ViewChild(PanelTitleDirective) public ptd: PanelTitleDirective;
}

@suite
class PanelsSpec extends DirectivesSpec<PanelsTestComponent> {

  public constructor() {
    super([PanelDirectivesModule], PanelsTestComponent);
  }

  @test
  public 'panel'(): void {
    this.minTest('panel');
    this.typeTest('panel', this.i.pd);
  }

  @test
  public 'upePanelBody'(): void {
    this.minTest('PanelBody');
  }

  @test
  public 'upePanelFooter'(): void {
    this.minTest('PanelBody');
  }

  @test
  public 'upePanelHeading'(): void {
    this.minTest('PanelBody');
  }

  @test
  public 'upePanelTitle'(): void {
    this.minTest('PanelTitle');
    this.textCenterTest('PanelTitle', this.i.ptd);
  }

}
