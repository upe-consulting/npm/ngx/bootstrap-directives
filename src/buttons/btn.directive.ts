import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import {
  AttrBoolean,
  BootstrapActiveDirective,
  BootstrapDisabledDirective,
  BootstrapTypeDirective,
  IBootstrapActiveDirective,
  IBootstrapDisabledDirective
} from '../bootstrap';

@Directive({selector: '[upeBtn]'})
export class BtnDirective extends BootstrapTypeDirective implements OnInit,
                                                                    IBootstrapDisabledDirective,
                                                                    IBootstrapActiveDirective {

  @Input() public active: AttrBoolean;
  @Input() public disabled: AttrBoolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['btn'], ['BUTTON', 'INPUT', 'A'], 'default');
  }

  @Input()
  public set block(value: AttrBoolean) {
    value || value === '' ? this.addClass('btn-block') : this.removeClass('btn-block');
  }

  private _type: string = 'button';

  @Input()
  public set type(value: string) {
    this.setAttribute('type', value);
    this._type = value;
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.setAttribute('role', 'button');
    this.type = this._type;
  }

}

ApplyMixins(BtnDirective, [BootstrapActiveDirective, BootstrapDisabledDirective]);
