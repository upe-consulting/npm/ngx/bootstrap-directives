import { NgModule } from '@angular/core';
import { ProgressBarDirective } from './progress-bar.directive';
import { ProgressDirective } from './progress.directive';

@NgModule({
  imports: [],
  exports: [
    ProgressDirective,
    ProgressBarDirective,
  ],
  declarations: [
    ProgressDirective,
    ProgressBarDirective,
  ],
  providers: [],
})
export class ProgressBarDirectivesModule {
}
