import { NgModule } from '@angular/core';
import { ListGroupItemHeadingDirective } from './list-group-item-heading.directive';
import { ListGroupItemTextDirective } from './list-group-item-text.directive';
import { ListGroupItemDirective } from './list-group-item.directive';
import { ListGroupDirective } from './list-group.directive';

@NgModule({
  imports: [],
  exports: [
    ListGroupDirective,
    ListGroupItemDirective,
    ListGroupItemHeadingDirective,
    ListGroupItemTextDirective,
  ],
  declarations: [
    ListGroupDirective,
    ListGroupItemDirective,
    ListGroupItemHeadingDirective,
    ListGroupItemTextDirective,
  ],
  providers: [],
})
export class ListGroupDirectivesModule {
}
