import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ApplyMixins } from '@upe/apply-class-mixins';
import {
  AttrBoolean,
  BootstrapDirective,
  BootstrapTextCenterDirective,
  IBootstrapTextCenterDirective
} from '../bootstrap';

@Directive({selector: '[upePanelTitle]'})
export class PanelTitleDirective extends BootstrapDirective implements IBootstrapTextCenterDirective {

  @Input() public center: AttrBoolean;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['panel-title'], ['H1', 'H2', 'H3', 'H4', 'H5', 'H6']);
  }

}

ApplyMixins(PanelTitleDirective, [BootstrapTextCenterDirective]);
