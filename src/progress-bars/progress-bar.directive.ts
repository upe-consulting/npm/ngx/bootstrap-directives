import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { AttrBoolean, BootstrapTypeDirective } from '../bootstrap';

@Directive({selector: '[upeProgressBar]'})
export class ProgressBarDirective extends BootstrapTypeDirective implements OnInit {

  @Input() public fraction: number = 2;
  private _min: number = 0;
  private _max: number = 100;

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['progress-bar']);
  }

  @Input()
  public set minWidth(value: number | string) {
    this.setStyle('min-width', typeof value === 'string' ? value : `${value}px`);
  }

  @Input()
  public set animated(value: AttrBoolean) {
    const tmpStriped = this._striped;
    this.striped = value || this._striped;
    this._striped = tmpStriped;
    value || value === '' ? this.addClass('active') : this.removeClass('active');
  }

  private _striped: boolean = false;

  @Input()
  public set min(value: number) {
    this._min = value;
    this.setAttribute('aria-valuemin', this._min.toFixed(this.fraction));
    if (this._progress < this._min) {
      this.progress = this._min;
    }
  }

  @Input()
  public set striped(value: AttrBoolean) {
    value || value === '' ? this.addClass('progress-bar-striped') : this.removeClass('progress-bar-striped');
    this._striped = value || value === '';
  }

  @Input()
  public set max(value: number) {
    this._max = value;
    this.setAttribute('aria-valuemax', this._max.toFixed(this.fraction));
    if (this._progress > this._max) {
      this.progress = this._max;
    }
  }

  private _progress: number = 0;

  @Input()
  public set progress(value: number) {
    if (value < this._min || value > this._max) {
      throw new Error('out of boundary');
    }
    this._progress = value;
    const progress = this._progress.toFixed(this.fraction);
    this.setAttribute('aria-valuenow', progress);
    this.setStyle('width', `${progress}%`);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.setAttribute('role', 'progressbar');
    this.progress = this._progress;
    this.min = this._min;
    this.max = this._max;
  }

}
