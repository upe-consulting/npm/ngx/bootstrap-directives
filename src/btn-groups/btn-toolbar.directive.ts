import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { BootstrapDirective } from '../bootstrap/bootstrap.directive';

@Directive({selector: '[upeBtnToolbar]'})
export class BtnToolbarDirective extends BootstrapDirective implements OnInit {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['btn-toolbar']);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.setAttribute('role', 'toolbar');
  }

}
