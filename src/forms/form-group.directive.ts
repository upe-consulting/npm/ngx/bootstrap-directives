import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { AttrBoolean, BootstrapSizeDirective, ValidationState } from '../bootstrap';

@Directive({selector: '[upeFormGroup]'})
export class FormGroupDirective extends BootstrapSizeDirective {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['form-group']);
  }

  @Input()
  public set hasError(value: AttrBoolean) {
    value || value === '' ? this.validationState = 'error' : this.validationState = null;
  }

  @Input()
  public set hasWarning(value: AttrBoolean) {
    value || value === '' ? this.validationState = 'warning' : this.validationState = null;
  }

  @Input()
  public set hasSuccess(value: AttrBoolean) {
    value || value === '' ? this.validationState = 'success' : this.validationState = null;
  }

  @Input()
  public set hasFeedback(value: AttrBoolean) {
    value || value === '' ? this.addClass('has-feedback') : this.removeClass('has-feedback');
  }

  private _validationState: ValidationState | null = null;

  @Input()
  public set validationState(value: ValidationState | null) {
    if (this._validationState !== null) {
      this.removeClass(`has-${this._validationState}`);
    }
    this._validationState = value;
    if (this._validationState !== null) {
      this.addClass(`has-${this._validationState}`);
    }
  }

}
