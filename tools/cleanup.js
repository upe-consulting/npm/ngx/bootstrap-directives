const del = require('del');
del(['src/**/*.ngsummary.json', 'src/**/*.ngfactory.ts']).then(paths => {
  if (paths.length) {
    console.log('Src Files and folders deleted:\n', paths.join('\n'));
  }
});
