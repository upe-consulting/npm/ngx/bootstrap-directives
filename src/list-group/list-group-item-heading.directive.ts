import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { BootstrapDirective } from '../bootstrap';

@Directive({selector: '[upeListGroupItemHeading]'})
export class ListGroupItemHeadingDirective extends BootstrapDirective {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['list-group-item-heading'], ['H1', 'H2', 'H3', 'H4', 'H5', 'H6']);
  }

}
