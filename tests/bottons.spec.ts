import { Component, ViewChild } from '@angular/core';
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { BtnDirective, ButtonDirectivesModule } from '../src/buttons';
import { DirectivesSpec } from './directives';

@Component({
  template: `
              <button upeBtn></button>`,
})
class ButtonsTestComponent {
  @ViewChild(BtnDirective) public bd: BtnDirective;
}

@suite
class BottonsSpec extends DirectivesSpec<ButtonsTestComponent> {

  public constructor() {
    super([ButtonDirectivesModule], ButtonsTestComponent);
  }

  @test
  public 'btn'(): void {
    const debugElement = this.minTest('btn');
    this.disabledTest('btn', this.i.bd);
    this.activeTest('btn', this.i.bd);

    expect(debugElement.classes['btn-block']).undefined;
    this.i.bd.block = true;
    expect(debugElement.classes['btn-block']).true;
    this.i.bd.block = false;
    expect(debugElement.classes['btn-block']).false;

    expect(debugElement.attributes.type).eq('button');

    const type     = 'submit';
    this.i.bd.type = type;
    expect(debugElement.attributes.type).eq(type);

  }

}
