import { Component, ViewChild } from '@angular/core';
import { suite, test } from 'mocha-typescript';
import {
  InputGroupAddonDirective,
  InputGroupBtnDirective,
  InputGroupDirective,
  InputGroupDirectivesModule,
} from '../src/input-groups';
import { DirectivesSpec } from './directives';

@Component({
  template: `
              <div upeInputGroup>
                <span upeInputGroupAddon></span>
                <span upeInputGroupBtn></span>
              </div>`,
})
class InputGroupsTestComponent {
  @ViewChild(InputGroupDirective) public igd: InputGroupDirective;
  @ViewChild(InputGroupAddonDirective) public igad: InputGroupAddonDirective;
  @ViewChild(InputGroupBtnDirective) public igbd: InputGroupBtnDirective;
}

@suite
class InputGroupsSpec extends DirectivesSpec<InputGroupsTestComponent> {

  public constructor() {
    super([InputGroupDirectivesModule], InputGroupsTestComponent);
  }

  @test
  public 'upeInputGroup'(): void {
    this.minTest('InputGroup');
    this.sizeTest('InputGroup', this.i.igd);
  }

  @test
  public 'upeInputGroupAddon'(): void {
    this.minTest('InputGroupAddon');
    this.visibleTest('InputGroupAddon', this.i.igad);
  }

  @test
  public 'upeInputGroupBtn'(): void {
    this.minTest('InputGroupBtn');
    this.visibleTest('InputGroupBtn', this.i.igbd);
  }

}
