import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { BootstrapSizeDirective } from '../bootstrap/bootstrap-size.directive';

@Directive({selector: '[upeBtnGroup]'})
export class BtnGroupDirective extends BootstrapSizeDirective implements OnInit {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['btn-group']);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.setAttribute('role', 'group');
  }

}
