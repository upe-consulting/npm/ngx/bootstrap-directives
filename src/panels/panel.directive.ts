import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { BootstrapTypeDirective } from '../bootstrap';

@Directive({selector: '[upePanel]'})
export class PanelDirective extends BootstrapTypeDirective {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['panel'], ['DIV'], 'default');
  }

}
