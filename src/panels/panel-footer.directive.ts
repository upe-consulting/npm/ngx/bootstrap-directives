import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { BootstrapDirective } from '../bootstrap';

@Directive({selector: '[upePanelFooter]'})
export class PanelFooterDirective extends BootstrapDirective {

  public constructor(r: Renderer2, er: ElementRef) {
    super(r, er, ['panel-footer']);
  }

}
