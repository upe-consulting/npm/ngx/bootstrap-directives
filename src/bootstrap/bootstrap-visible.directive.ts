import { BootstrapDirective } from './bootstrap.directive';

export interface IBootstrapVisibleDirective {
  visible: boolean;
}

export abstract class BootstrapVisibleDirective extends BootstrapDirective implements IBootstrapVisibleDirective {
  public set visible(value: boolean) {
    this.ifValue = value;
  }
}
