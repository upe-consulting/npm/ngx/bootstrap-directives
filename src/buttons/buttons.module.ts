import { NgModule } from '@angular/core';
import { BtnDirective } from './btn.directive';

@NgModule({
  imports: [],
  exports: [
    BtnDirective,
  ],
  declarations: [
    BtnDirective,
  ],
  providers: [],
})
export class ButtonDirectivesModule {
}
