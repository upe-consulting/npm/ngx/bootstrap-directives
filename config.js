export const LIBRARY_NAME = 'ngx-module';
export const PATH_SRC     = 'src/';
export const PATH_DIST    = 'dist/';
export const EXTERNAL     = ['@angular/core'];
export default {LIBRARY_NAME, PATH_SRC, PATH_DIST, EXTERNAL};