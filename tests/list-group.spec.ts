import { Component, ViewChild } from '@angular/core';
import { suite, test } from 'mocha-typescript';
import { ListGroupDirectivesModule, ListGroupItemDirective } from '../src/list-group';
import { DirectivesSpec } from './directives';

@Component({
  template: `
              <ul upeListGroup>
                <li upeListGroupItem>
                  <h4 upeListGroupItemHeading></h4>
                  <p upeListGroupItemText></p>
                </li>
              </ul>`,
})
class ListGroupTestComponent {
  @ViewChild(ListGroupItemDirective) public lgid: ListGroupItemDirective;
}

@suite
class PanelsSpec extends DirectivesSpec<ListGroupTestComponent> {

  public constructor() {
    super([ListGroupDirectivesModule], ListGroupTestComponent);
  }

  @test
  public 'upeListGroup'(): void {
    this.minTest('ListGroup');
  }

  @test
  public 'upeListGroupItem'(): void {
    this.minTest('ListGroupItem');
    this.disabledTest('ListGroupItem', this.i.lgid);
    this.typeTest('ListGroupItem', this.i.lgid, false);
  }

  @test
  public 'upeListGroupItemHeading'(): void {
    this.minTest('ListGroupItemHeading');
  }

  @test
  public 'upeListGroupItemText'(): void {
    this.minTest('ListGroupItemText');
  }

}
