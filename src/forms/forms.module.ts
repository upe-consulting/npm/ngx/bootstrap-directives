import { NgModule } from '@angular/core';
import { CheckboxInlineDirective } from './checkbox-inline.directive';
import { CheckboxDirective } from './checkbox.directive';
import { ControlLabelDirective } from './control-label.directive';
import { FormControlStaticDirective } from './form-control-static.directive';
import { FormControlDirective } from './form-control.directive';
import { FormGroupDirective } from './form-group.directive';
import { FormHorizontalDirective } from './form-horizontal.directive';
import { FormInlineDirective } from './form-inline.directive';
import { HelpBlockDirective } from './help-block.directive';
import { RadioInlineDirective } from './radio-inline.directive';
import { RadioDirective } from './radio.directive';

@NgModule({
  imports: [],
  exports: [
    CheckboxInlineDirective,
    CheckboxDirective,
    ControlLabelDirective,
    FormControlDirective,
    FormControlStaticDirective,
    FormGroupDirective,
    FormHorizontalDirective,
    FormInlineDirective,
    HelpBlockDirective,
    RadioDirective,
    RadioInlineDirective,
  ],
  declarations: [
    CheckboxInlineDirective,
    CheckboxDirective,
    ControlLabelDirective,
    FormControlDirective,
    FormControlStaticDirective,
    FormGroupDirective,
    FormHorizontalDirective,
    FormInlineDirective,
    HelpBlockDirective,
    RadioDirective,
    RadioInlineDirective,
  ],
  providers: [],
})
export class FormDirectivesModule {
}
